test:	tmp/linux.pack tmp/test-repo.git
	cat tmp/linux.pack | (cd tmp/test-repo.git && time git index-pack --stdin)

tmp/linux.pack:	tmp
	curl https://s3.amazonaws.com/gitlab-internal-file-hosting/test-data/pack-78486715cddada143192c51c8e82ea14369b91df.pack > $@

tmp/test-repo.git:	tmp
	git init --bare $@

tmp:
	mkdir tmp

clean:
	rm -rf tmp
